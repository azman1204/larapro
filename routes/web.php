<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;

Route::middleware(['audit.trail', 'login'])->group(function() {
    Route::view('/dashboard', 'dashboard.main');

    // film
    // any - support get dan post
    Route::any('/film-list', [FilmController::class, 'list']);
});

Route::middleware(['audit.trail'])->group(function() {
    include 'login.php';
});
