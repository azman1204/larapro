<?php
use App\Http\Controllers\LoginController;
// show login page
Route::get('/', [LoginController::class, 'login']);
// handle post login form
Route::post('/login', [LoginController::class, 'auth']);
Route::get('/logout', [LoginController::class, 'logout']);
