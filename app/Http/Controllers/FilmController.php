<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Film;

class FilmController extends Controller {
    // list all film
    function list(Request $req) {
        // $films = Film::all();
        if ($req->isMethod('post')) {
            // click btn search
            $title = $req->title;
            //$films = Film::where('title', 'like', "%$title%")->paginate(20);
            $q = Film::where('title', 'like', "%$title%"); // return query obj

            if (! empty($req->description)) {
                $description = $req->description;
                $q = $q->where('description', 'like', "%$description%");
            }

            $films = $q->paginate(20);
        } else {
            // click pd menu link
            $films = Film::paginate(20);
        }

        return view('film.list', compact('films'));
    }
}
