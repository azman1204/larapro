<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    // show login page
    function login() {
        return view('login.login');
    }

    // authenticate user. onsubmit login form
    function auth(Request $req) {
        $credentials = [
            'username'  => $req->user_id,
            'password' => $req->password
        ];

        if(\Auth::attempt($credentials)) {
            // berjaya
            //echo 'Berjaya';
            return redirect('/dashboard');
        } else {
            //gagal
            //echo 'gagal';
            $err = 'Login Gagal';
            return view('login.login', compact('err'));
        }
        // return "Hello";
    }

    // logout user
    function logout() {
        \Auth::logout();
        session()->flush(); // delete semua session
        return redirect('/');
    }

    // update profile
    function profile() {

    }
}
