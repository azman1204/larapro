<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;

class AuditTrail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $at = new \App\Models\AuditTrail();
        $at->url = $request->path();

        if ($at->url == 'login') {
            // submit login form
            $data = $request->except('password');
        } else {
            $data = $request->all();
        }

        if (\Auth::check()) {
            // dah login
            $at->user_id = \Auth::user()->username;
        }

        $at->data = json_encode($data); // all() return semua data yg disubmit dlm bentuk array
        $at->ip = $request->ip();
        $at->prev_url = url()->previous();
        $at->method = $request->method(); // post form / click link
        $at->agent = $request->server('HTTP_USER_AGENT');
        $at->save();

        return $next($request);
    }
}
