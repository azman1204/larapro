<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //var_dump(\Auth::user());exit;
        // check user dah login atau belum
        if (\Auth::check()) {
            // user dah login. teruskan...
            return $next($request);
        } else {
            // user belum login. / cubaan hacking
            return redirect('/');
        }
    }
}
