<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// audit_trails
class AuditTrail extends Model
{
    use HasFactory;
}
