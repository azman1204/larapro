<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmActor extends Model
{
    use HasFactory;
    protected $table = 'film_actor';

    public function actor() {
        return $this->belongsTo(\App\Models\Actor::class, 'actor_id', 'actor_id');
    }
}
