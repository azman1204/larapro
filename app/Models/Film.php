<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    // jika nama table plural (films), maka setting dibawah x perlu
    protected $table='film';
    protected $primaryKey = 'film_id';

    // nama method ini boleh bg apa2 shj nama yg sesuai
    // hasOne(Model, fk, pk)
    public function filmCategory() {
        return $this->hasOne(\App\Models\FilmCategory::class, 'film_id', 'film_id');
    }

    public function filmActor() {
        return $this->hasMany(\App\Models\FilmActor::class, 'film_id', 'film_id');
    }
}
