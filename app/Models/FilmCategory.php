<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmCategory extends Model
{
    use HasFactory;
    protected $table = 'film_category';

    public function category() {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'category_id');
    }
}
