@extends('layout.master')
@section('title', 'Film List')
@section('content')

<form action="/film-list" method="POST">
    @csrf
    <div class="row">
        <div class="col-md-1">Title</div>
        <div class="col-md-4"><input type="text" name="title" class="form-control"></div>
        <div class="col-md-1">Description</div>
        <div class="col-md-4"><input type="text" name="description" class="form-control"></div>
        <div class="col-md-1"><input type="submit" value="Search" class="btn btn-primary"></div>
    </div>
</form>

<table class="table table-bordered table-striped table-hover mt-1">
    <thead>
        <tr>
            <th>No.</th>
            <th>Title</th>
            <th>Description</th>
            <th>Category</th>
            <th>Actors</th>
        </tr>
    </thead>
    <tbody>
        @php $no = $films->firstItem() @endphp
        @foreach($films as $film)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $film->title }}</td>
            <td>{{ $film->description }}</td>
            <td>{{ $film->filmCategory->category->name }}</td>
            <td>
                @foreach($film->filmActor as $fa)
                    <li>{{ $fa->actor->first_name }} {{ $fa->actor->last_name }}</li>
                @endforeach
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $films->links() }}
@endsection
